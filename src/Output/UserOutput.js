import React from 'react';

const cardStyles = {
    boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)",
    float: 'left',
    width: '250px',
    margin: '10px',
    padding: '5px'
}

const UserOutput = (props) => {
    return (
        <div style={cardStyles}>
            <h4>Username: {props.username}</h4>
            <p id="paragraph1">{props.paragraphs[0]}</p>
            <p id="paragraph2">{props.paragraphs[1]}</p>
        </div>
    )
}

export default UserOutput
