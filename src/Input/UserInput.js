import React from 'react';

const UserInput = (props) => {
    return (
        <div className="UserInput">
            <label htmlFor="input">Input: </label>
            <input type="text" name="input" onChange={props.onChangeUsername} value={props.username} />
        </div>
    )
}

export default UserInput

